$(function() {
  var swiperBrand = new Swiper('.brand-slider', {
    speed: 600,
    slidesPerView: 1,
    loop: true,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  });

  var swiperBrand = new Swiper('.slider-excursion', {
    speed: 600,
    slidesPerView: 1,
    loop: true,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  });

  var swiperRecommendation = new Swiper('.recommendation-slider', {
    slidesPerView: 1,
    loop: true,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  });


  var swiperTeam = new Swiper('.team-slider', {
    slidesPerView: 3,
    loop: true,
    breakpoints: {
      320: {
        slidesPerView: 1,
        spaceBetween: 10
      },

      640: {
        slidesPerView: 1,
        spaceBetween: 10
      },

      769: {
        slidesPerView: 2,
        spaceBetween: 10
      },

      1024: {
        slidesPerView: 2,
        spaceBetween: 10
      },

    },
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  });

  
});