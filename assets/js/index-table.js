var saveElement = $("#save-settings");
var saveFixedElement = $("#save-settings-fixed");

if($(saveElement).position().top < $(window).height()) {
	$(saveFixedElement).hide();
} else {
	$(saveFixedElement).show();
}

$(window).scroll(function(){
	var scroll = $(saveElement).position().top - $(window).height();
	if ($(window).scrollTop() > scroll) {
		$(saveFixedElement).hide();
	} else {
		$(saveFixedElement).show();
	}
});

$('.btn-left').mousedown(function(){
	var leftPos = $('.tab-pane').scrollLeft();
	$(".tab-pane").animate({scrollLeft: leftPos -250}, 500);
});

$('.btn-right').mousedown(function(){
	var leftPos = $('.tab-pane').scrollLeft();
	$(".tab-pane").animate({scrollLeft: leftPos + 250}, 500);
});

