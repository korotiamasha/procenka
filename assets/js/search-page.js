/*more-item*/
$(".more-item").click(function(){
	var article = $(this).attr('data-article');
	var tableItems = $(".search-table-item[data-article='" + article + "']:not(:first)");  

	if ($(this).attr('data-open')) {
		tableItems.addClass('search-table-item-hide').removeClass('border-0');
		$(this).removeAttr('data-open').text('Eщё ' + tableItems.length);
	} else {
		tableItems.removeClass('search-table-item-hide').addClass('border-0');	
		$(this).attr('data-open', 'true').text('Скрыть');
	}
});


/*Filter button*/
$(".filter-item").click(function(){

	if ($(this).attr('data-open')) {
		$('.search-table').show();
		$('.table-producer').hide();
		$(this).removeAttr('data-open').removeClass('filter-item2').text("Свернуть по производителям");
	} else {
		$('.search-table').hide();
		$('.table-producer').show();
		$(this).attr('data-open', 'true').addClass('filter-item2').text("Развернуть производителей");
	}
});
