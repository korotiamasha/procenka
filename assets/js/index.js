$('.header .menu-btn').click(function() {
	var clicks = $(this).data('clicks');

	if (clicks) {
		$('.min-header-menu').show();
		$(this).addClass('close-menu');
		$('body').addClass('modal-open');
	} else {
		$(this).removeClass('close-menu');
		$('.min-header-menu').hide();
		$('body').removeClass('modal-open');
	}

	$(this).data("clicks", !clicks);
});

$('.select').selectpicker();



var substringMatcher = function(strs) {
	return function findMatches(q, cb) {
		var matches, substringRegex;

    // an array that will be populated with substring matches
    matches = [];

    // regex used to determine if a string contains the substring `q`
    substrRegex = new RegExp(q, 'i');

    // iterate through the pool of strings and for any string that
    // contains the substring `q`, add it to the `matches` array
    $.each(strs, function(i, str) {
    	if (substrRegex.test(str)) {
    		matches.push(str);
    	}
    });

    cb(matches);
};
};


var states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California',
'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii',
'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana',
'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota',
'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire',
'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota',
'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island',
'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont',
'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'
];

$('#header-search').typeahead({
	minLength: 0,
	highlight: true,
},
{
	name: 'states',
	source: substringMatcher(states),
	templates: {
		empty : "<div>no results</div>",
		footer: "<div class=\"d-flex tt-footer\"><div><a href="+ '#' +">Все результаты</a></div><div class=\"label\">ENTER</div><button class=\"enter\"></button></div>",
	}
});