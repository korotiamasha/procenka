/*drap and drop document*/

var $form = $('.upload-file');

var droppedFiles = false;

$form.on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
	e.preventDefault();
	e.stopPropagation();
})
.on('dragover dragenter', function() {
	$form.addClass('is-dragover');
	$form.find('.dropzone-start').hide();
})
.on('dragleave dragend drop', function() {
	$form.removeClass('is-dragover');
	$form.find('.dropzone-start').show();
})
.on('drop', function(e) {
	$form.find('.dropzone-start').hide();
	$form.find('.dropzone-end').show();
	droppedFiles = e.originalEvent.dataTransfer.files;
});

$form.find('input[type="file"]').on('change', function(e) {
	$form.trigger('submit');
	$form.find('.dropzone-start').hide();
	$form.find('.dropzone-end').show();
});

