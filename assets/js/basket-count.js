$(document).ready(function(){
    $(".btn-minus").on("click",function(){
        var now = $(this).parent().find('input').val();
        if ($.isNumeric(now)){
            if (parseInt(now) -1> 0)
                { now--;}
            $(this).parent().find('input').val(now);
        }
    })            
    $(".btn-plus").on("click",function(){
        var now = $(this).parent().find('input').val();
        if ($.isNumeric(now)){
            $(this).parent().find('input').val(parseInt(now)+1);
        }
    })                        
})